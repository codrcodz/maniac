## [1.0.1](https://gitlab.com/dreamer-labs/maniac/maniac/compare/v1.0.0...v1.0.1) (2019-10-30)


### Bug Fixes

* removed unresolved vars in name parameters ([3bf1062](https://gitlab.com/dreamer-labs/maniac/maniac/commit/3bf1062))

# 1.0.0 (2019-10-30)


### Features

* Bootstrap semantic-release ([8148d6f](https://gitlab.com/dreamer-labs/maniac/maniac/commit/8148d6f)), closes [#5](https://gitlab.com/dreamer-labs/maniac/maniac/issues/5)
* Update image name used for testing ([6ec0399](https://gitlab.com/dreamer-labs/maniac/maniac/commit/6ec0399))
* Updated image name... again ([727659d](https://gitlab.com/dreamer-labs/maniac/maniac/commit/727659d))
