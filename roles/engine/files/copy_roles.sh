#!/bin/bash

# This script is not generic and is designed to work exclusively with the Ansible ManIAC role

ll_roles_tld="${1}"
role_path="${2}"
old_role_list="$(ls ${ll_roles_tld})"

# Ensure exactly two parameters are passed to script from Ansible task calling it
if [[ "${#}" != "2" ]]; then
  echo "[FAIL] Script requires exactly two arguments, first => ll_roles_tld, second => role_path; exiting." 1>&2;
  exit 1; 
fi;

# Ensure ll_role_tld is actually a valid directory
if [[ ! -d "${ll_roles_tld}" ]]; then
  echo "[FAIL] ll_roles_tld (first arg passed to script) doesn't exist, or not a directory; exiting." 1>&2;
  exit 1;
fi;

# Ensure role_path is actually a valid directory
if [[ ! -d "${role_path}" ]]; then
  echo "[FAIL] role_path (second arg passed to script) doesn't exist, or not a directory; exiting." 1>&2;
  exit 1;
fi;

# Parse each role dep top-level directory and copy each one into roles top-level directory
for host in $(ls ${role_path}/.tmp/);
do
  for dep in $(ls ${role_path}/.tmp/${host}/dest/);
    do
      if [[ -d "${role_path}/.tmp/${host}/dest/${dep}/" ]]; then
	echo "[INFO] Attempting to copy dep ("${host}"/"${dep}") into roles directory ("${ll_roles_tld}")."
        mv -t "${ll_roles_tld}" "${role_path}/.tmp/${host}/dest/${dep}/" 2>/dev/null;
	if [[ ! -d "${ll_roles_tld}/${dep}" ]]; then
	  echo "[FAIL] Failed to copy dep ("${host}"/"${dep}") into roles directory ("${ll_roles_tld}"/"${dep}"); exiting." 1>&2;
	  exit 1;
	else
          echo "[INFO] Successfully copied dep ("${host}"/"${dep}") into roles directory ("${ll_roles_tld}"/"${dep}").";
	fi;
      else
	echo "[FAIL] Role dep ("${role_path}"/.tmp/"${host}"/dest/"${dep}"/) doesn't exist, or not a directory; exiting." 1>&2;
	exit 1;
      fi;
  done;
done;

# Snag new role list and print both new and old role list for manual comparison
new_role_list="$(ls ${ll_roles_tld})"
echo -e "[INFO] Old role list:\n${old_role_list}"
echo -e "[INFO] New role list:\n${new_role_list}"

# Check to see if new roles added to roles top-level directory, and return appropriate code to Ansible
if [[ "${new_role_list}" == "${old_role_list}" ]]; then
  echo "[SUCCESS] Script completed with no role list changes detected; exiting 0.";
  exit 0;
else
  echo "[SUCCESS] Script completed with role list changes detected; exiting 2.";
  exit 2;
fi
