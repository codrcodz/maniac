FROM alpine:latest as maniac
ENV BUILD_DEPS="python3-dev build-base automake autoconf libffi-dev openssl-dev"
ENV SYS_DEPS="python3 python ca-certificates coreutils openssh-client git bash curl docker-cli python python3 py3-pip py-pip ${BUILD_DEPS}"
ENV PY3_DEPS="ansible molecule docker"
ENV PY2_DEPS="docker-py"
ENV BINS="python3 python molecule ansible ansible-lint"
RUN { echo -e "\n[INFO] Displaying distro details:"; \
      cat /etc/*release || exit 1; } && \
    { echo -e "\n[INFO] Updating system package list:"; \
      apk -v update || exit 1; } && \
    { echo -e "\n[INFO] Upgrading installed packages:"; \
      apk -v upgrade || exit 1;} && \
    { { echo '[global]' > /etc/pip.conf || exit 1; } && \
      { echo 'disable-pip-version-check = True' >> /etc/pip.conf || exit 1; }; } && \
    { for SYS_DEP in ${SYS_DEPS}; do \
        echo -e "\n[INFO] Installing sys dep: ${SYS_DEP}"; \
        apk -v add --no-cache "${SYS_DEP}"; \
      done || exit 1; } && \
    { echo -e "\n[INFO] Installing Python3 deps: ${PY3_DEPS}" && \
      pip3 install --no-cache-dir ${PY3_DEPS}; } || { exit 1; } && \
    { echo -e "\n[INFO] Installing Python2 dep: ${PY2_DEPS}" && \
      pip install --no-cache-dir ${PY2_DEPS}; } || { exit 1; } && \
    { echo -e "\n[INFO] Updating outdated py3 packages:"; \
      for PKG in $(pip3 list --outdated --format=freeze); do \
        echo -e "${PKG}: $(pip3 install --upgrade ${PKG})\n"; \
      done; } && \
    { echo -e "\n[INFO] Updating outdated py2 packages:"; \
      for PKG in $(pip list --outdated --format=freeze); do \
        echo -e "${PKG}: $(pip install --upgrade ${PKG})\n"; \
      done; } && \
    { echo -e "\n[INFO] Displaying Python3 packages installed list:"; \
      pip3 freeze || exit 1; } && \
    { echo -e "\n[INFO] Displaying Python2 packages installed list:"; \
      pip freeze || exit 1; } && \
    { for BUILD_DEP in ${BUILD_DEPS}; do \
        echo -e "\n[INFO] Deleting build dep: ${BUILD_DEP}"; \ 
        apk -v del ${BUILD_DEP}; \
      done || exit 1; } && \
    { for BIN in ${BINS}; do \
        echo -e "\n[INFO] Displaying location and version of bin: ${BIN}"; \
        { which ${BIN} || exit 1; }; \
      done || exit 1; } && \
    { echo -e "\n[INFO] Displaying sys packages stats and installed list:"; \
      { apk -v stats; apk -v list --installed; } || exit 1; };
WORKDIR /workdir
CMD /bin/bash
